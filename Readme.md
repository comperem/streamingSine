## This is a simple Bokeh-based sliding window time-series data plotter

This GitLab repo contains 2 files with code:
- `streamingSine_v3.py`       - Bokeh-based sliding window time-series data plotter
- `udpSender_srt_method_4.py` - soft-real-time udp sender with example data to plot

### Description
This software is intended to plot incoming sensor data along a time-axis.
The primary application is `streamingSine_v3.py` which is a python-based
Bokeh server script. Links for more about [Bokeh](https://bokeh.org/)
generally and [Bokeh Server functionality]
(https://docs.bokeh.org/en/latest/docs/user_guide/server/server_introduction.html#ug-server-introduction).

The reason I like `Bokeh serve` scripts is the ability to program (a) in Python
and (b) in the browser window. It's quite similar to
Arduino's `setup()` and `loop()` paradigm, except with Python running in a web browser.

There is an example data generator called `udpSender_srt_method_4.py` that sends
udp/ip messages at a specified intervals to simulate 3 channels of incoming data.

The Bokeh server script has a thread that listens for incoming data and plots
it along a time axis, in seconds, with a variable number of points.

If data arrives more quickly than can be captured at the plotter's update interval,
then intermediate data points are skipped, with the number of skipped points indicated.

Default settings for the data plotter are 50ms update intervals.

The example udp sender can send example data at long intervals like 1(s) or as
short as 0.001(s) intervals, which is 1000Hz, quite reliably from a Raspberry Pi.

Soft-real-time is achieved using what I've called method #4 which works quite well
in Python in user-space without specialized hardware or kernel interaction.
No delay is accumulated over short or extended periods and
intervals as short as 1ms (1 millisecond) or better are achieved reliably in stock
linux Kernels. Soft-real-time is even achieved in Windows-Subsystem-for-Linux (WSL).

### Data rates explanation in the screenshot below
If the data sending interval is 0.001(s) and the Bokeh browser update interval
is 0.05(s), then the `streamingSine_v3.py` application skips 50 samples,
which is 0.05/0.001. This is computed dynamically on the Bokeh side as data arrives.

The example shown below illustrates `dt_display` specified at 0.05(s),
`skipCnt` auto-detected between 50 and 51, and `dt_data` auto-detected near 0.001(s),
which corresponds to the 1ms sender's send interval.

![3 channels of streaming data](screenshot_streamingSine_v3.png "3 channels of streaming data")

Logging is enabled with a GUI button that triggers logging using
Python [DictWriter](https://docs.python.org/3/library/csv.html).

When logging to a file in the local directory, the logfile size is updated every
second in the uptime bar to remind and confirm it's being written at the high frequency udp rate.

![logging data to file](screenshot_streamingSine_v3_stop_b.png "logging data to file")


## License:
Both codes are released as free, open-source software under the
[Gnu Public License version 3](https://www.gnu.org/licenses/gpl-3.0.en.html)
, or GPLv3 license.





***
  Marc Compere, Ph.D.  
  comperem@erau.edu  
  created : 08 Jul 2017  
  modified: 07 Jan 2023
