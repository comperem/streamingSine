#!/usr/bin/env python3
#
# simulate 3 channels of sensor data, with noise
#
# example usage:
#   ./udpSender_srt_method_4.py                         (send default interval of 0.05(s) to localhost)
#   ./udpSender_srt_method_4.py 0.001                   (send at 0.001(s) intervals       to localhost)
#   ./udpSender_srt_method_4.py 0.001 192.168.1.158     (send at 0.001(s) intervals to this IP address)
#
# soft real time with no drift over long durations
# method: using a thread timer and computes waitTime based on absolute time error
# BEST METHOD with drift-free looptime over multiple hours, linux or WSL
#
# timer example from: https://stackoverflow.com/a/3393759/7621907
#
# this runs fine as standard user but some improvement may come with superuser
# priveleves and highest kernel scheduler priority:
#     sudo nice -n -19 ./udpSender_srt_method_4.py
#
# Marc Compere, comperem@erau.edu,
# created : 26 Jul 2021
# modified: 07 Jan 2023
#
# --------------------------------------------------------------
# Copyright 2017 - 2023 Marc Compere
#
# This file is streamingSine_v3.py which is open source software
# licensed under the GNU GPLv3.
#
# This file is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3
# as published by the Free Software Foundation.
#
# This file is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License version 3 for more details.
#
# A copy of the GNU General Public License is included with MoVE
# in the COPYING file, or see <https://www.gnu.org/licenses/>.
# --------------------------------------------------------------

import sys
import threading
import time
import logging
import code # for dropping into python shell with: code.interact(local=dict(globals(), **locals()))
import math # for floor()
import socket
#import struct
import msgpack
import numpy as np
import random # for noise

logging.basicConfig(level=logging.DEBUG,format='[%(levelname)s] (%(threadName)-10s) %(message)s',)


#dt = 0.005 #0.005 #1
dt = 0.05 # (s)
N  = math.floor(1/dt) # default to console output every dt delay
if len(sys.argv)>=2:
    dt   = float(sys.argv[1]) # (s) loop delay
    N = math.floor(1/dt)
    print('assigning dt={0} and N={1}'.format(dt,N))
    time.sleep(1)

remote_ip = '127.0.0.1' #sys.argv[1]
udp_port  = 8000        #int( sys.argv[2] )
if len(sys.argv)>=3:
    remote_ip = sys.argv[2] # 192.168.1.158
print("udp target [{0}:{1}]".format(remote_ip, udp_port))


tNow=time.time()
tStart=time.time()
loopErrAccum=0.0
cnt=0
time.sleep(dt)

debug=0 # (0/1)

writeFile=False
if writeFile==True:
    # ---------------- file -------------------
    # https://docs.python.org/3/library/time.html#time.strftime
    dateStr = time.strftime("srt_method_4_%Y_%m_%d__%H_%M_%S")
    fileName = dateStr + ".csv"
    print( 'opening fileName for writing .csv data [{0}]...'.format(fileName) , end='') # no newline
    myFile = open(fileName,'w')
    print('done.')
    # ----------------        -----------------



# open a udp socket
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # udp/ip



t0 = time.time()

def loop(tNow,cnt,N,loopErrAccum):
    
    t  = time.time() - t0
    
    # create Mx,My,Mz values as a function of time
    Mx_amp = 5    # (V)    100  # (uT)
    My_amp = 7    # (V)    300  # (uT)
    Mz_amp = 2    # (V)    1000 # (uT)
    freq   = 1 #20   # (Hz) sine wave frequency
    Mx_offset = +2     # (V)       250  # (uT) sine wave vertical offset
    My_offset = -4     # (V)      -800  # (uT) sine wave vertical offset
    Mz_offset =  2     # (V)      1000  # (uT) sine wave vertical offset
    
    noise = 0.07*random.uniform(-1.0,+1.0) # (V) noise model
    noise_x = noise; noise_y = noise; noise_z = noise
    
    phi_x = 0; phi_y = 0; phi_z = 0 # (rad) phase angles
    
    Mx = Mx_amp*np.sin(2*np.pi*freq*t + phi_x) + Mx_offset + noise_x # y=A*sin(omega*t)+offset, omega=2*pi*freq, omega in rad/s, freq in Hz
    My = My_amp*np.sin(2*np.pi*freq*t + phi_y) + My_offset + noise_y
    Mz = Mz_amp*np.sin(2*np.pi*freq*t + phi_z) + Mz_offset + noise_z
    
    #time.sleep(0.2*dt) # some computation that takes time
    #Mx = 1.23 + cnt
    #My = 4.56 + cnt
    #Mz = 7.89 + cnt
    
    myData={'t':t, 'Mx':Mx, 'My':My, 'Mz':Mz}
    if (cnt%(N/5)==0):
        print('cnt={0}, '.format(cnt),end='')
        print(",   ".join("{0}:{1:<0.3f}".format(k, v) for k, v in myData.items()))
    
    msg = msgpack.packb(myData)
    try:
        nBytesSent = sock.sendto(msg,(remote_ip, udp_port))
    except:
        print("Unexpected error:", sys.exc_info()[0])
        raise # raise the error and stop execution
    
    
    # soft real time calcs
    cnt+=1
    tLast=tNow
    tNow=time.time()
    
    elapsedTime  = tNow-tStart          # absolute measurement from tStart
    absErr       = elapsedTime - cnt*dt # absolute error; also happens to be the loop's real computation time estimate
    waitTime=max( 0.0 , dt-absErr )
    
    # none of these are required but are helpful for common output with other srt_method_*.py methods
    loopTime     = tNow-tLast
    loopErr      = dt - loopTime
    loopErrAccum = loopErrAccum + loopErr
    srtMargin = 100*(waitTime)/dt
    
    if ((cnt%N==0) and (debug>0)):
        writeStr='t,{0:< 20.8f}, elapsed time=,{1:0.6f}, loopTime=,{2:0.12f}, loopErr=,{3:>12.8f}, loopErrAccum=,{4:>12.8f}, srt margin=,{5:-6.2f},(%), waitTime={6}, absErr={7}, cnt={8}'\
                 .format(time.time(), elapsedTime,          loopTime,            loopErr,             loopErrAccum,             srtMargin, waitTime, absErr, cnt)
        logging.debug(writeStr)
        if writeFile==True:
            myFile.write(writeStr+'\n')
            myFile.flush()
    
    threading.Timer(waitTime, loop, [tNow,cnt,N,loopErrAccum]).start()
    



loop(tNow,cnt,N,loopErrAccum)















# pi@falconDev:~/srt_methods $ sudo nice -n -19 ./srt_method_4.py 
# 	elapsed time=,1.201422, loopTime=,1.201427459717, loopErr=, -0.20142746, loopErrAccum=, -0.20142746, srt margin=, 79.86,(%), waitTime=0.7985780239105225, absErr=0.20142197608947754
# 	elapsed time=,2.202195, loopTime=,1.000773429871, loopErr=, -0.00077343, loopErrAccum=, -0.20220089, srt margin=, 79.78,(%), waitTime=0.797804594039917, absErr=0.202195405960083
# 	elapsed time=,3.201628, loopTime=,0.999432563782, loopErr=,  0.00056744, loopErrAccum=, -0.20163345, srt margin=, 79.84,(%), waitTime=0.7983720302581787, absErr=0.2016279697418213
# 	elapsed time=,4.201267, loopTime=,0.999638795853, loopErr=,  0.00036120, loopErrAccum=, -0.20127225, srt margin=, 79.87,(%), waitTime=0.7987332344055176, absErr=0.20126676559448242
# 	elapsed time=,5.201379, loopTime=,1.000112295151, loopErr=, -0.00011230, loopErrAccum=, -0.20138454, srt margin=, 79.86,(%), waitTime=0.7986209392547607, absErr=0.20137906074523926
# 	elapsed time=,6.201327, loopTime=,0.999947547913, loopErr=,  0.00005245, loopErrAccum=, -0.20133209, srt margin=, 79.87,(%), waitTime=0.7986733913421631, absErr=0.20132660865783691
# 	elapsed time=,7.201506, loopTime=,1.000179052353, 
# 
# then after 13.8 hours the elapsed time is still reported on the same 0.201 fraction of a second
# 
#   elapsed time=,49525.201155, loopTime=,0.999917984009, loopErr=,  0.00008202, loopErrAccum=, -0.20116019, srt margin=, 79.88,(%), waitTime=0.7988452911376953, absErr=0.2011547088623047
# 	elapsed time=,49526.201184, loopTime=,1.000029325485, loopErr=, -0.00002933, loopErrAccum=, -0.20118952, srt margin=, 79.88,(%), waitTime=0.7988159656524658, absErr=0.20118403434753418
# 	elapsed time=,49527.201934, loopTime=,1.000749826431, loopErr=, -0.00074983, loopErrAccum=, -0.20193934, srt margin=, 79.81,(%), waitTime=0.7980661392211914, absErr=0.2019338607788086
# 	elapsed time=,49528.201184, loopTime=,0.999249696732, loopErr=,  0.00075030, loopErrAccum=, -0.20118904, srt margin=, 79.88,(%), waitTime=0.798816442489624, absErr=0.20118355751037598
# 	elapsed time=,49529.201109, loopTime=,0.999925851822, loopErr=,  0.00007415, loopErrAccum=, -0.20111489, srt margin=, 79.89,(%), waitTime=0.7988905906677246, absErr=0.2011094093322754
# 	elapsed time=,49530.201142, loopTime=,1.000032901764, loopErr=, -0.00003290, loopErrAccum=, -0.20114779, srt margin=, 79.89,(%), waitTime=0.7988576889038086, absErr=0.2011423110961914
# 	elapsed time=,49531.201344, loopTime=,1.000201702118, loopErr=, -0.00020170, loopErrAccum=, -0.20134950, srt margin=, 79.87,(%), waitTime=0.7986559867858887, absErr=0.20134401321411133
# 	elapsed time=,49532.201254, loopTime=,0.999910116196, loopErr=,  0.00008988, loopErrAccum=, -0.20125961, srt margin=, 79.87,(%), waitTime=0.79874587059021, absErr=0.20125412940979004

