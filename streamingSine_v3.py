# streamingSine.py - provides an animated sine wave updated in the
# browser window as a function of system time, y=A*sin(omega*t)+offset
# 
# This is intended as a starting point for plotting live streaming time-series data
# from a network data source, such as an Internet-of-Things (Iot) device
# with sensors or a vehicle transmitting live streaming telemetry.
#
# This example has no incoming data for simplicity as a bokeh server app
# but could be added in my_periodic_callback() to replace the y=A*sin(omega*t)
#
# The updates pesented in this simple example are single points. Multi-point updates can be achieved in sliding_window()
#
# run with this at an Anaconda command prompt:
#    bokeh serve --show streamingSine_v3.py
# or:
#    bokeh serve --show --allow-websocket-origin=192.168.1.107:5006 streamingSine_v2.py
#    bokeh serve        --allow-websocket-origin=72.239.115.57:5006 streamingSine_v2.py
#
# Colors can be any of the 147 W3 CSS names: https://www.w3schools.com/colors/colors_names.asp
#
# watch udp drop count: watch -n 1 -d 'cat /proc/net/udp'
#
# Marc Compere, comperem@gmail.com
# created : 08 Jul 2017
# modified: 07 Jan 2023
#
# --------------------------------------------------------------
# Copyright 2017 - 2023 Marc Compere
#
# This file is streamingSine_v3.py which is open source software
# licensed under the GNU GPLv3.
#
# This file is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 3
# as published by the Free Software Foundation.
#
# This file is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License version 3 for more details.
#
# A copy of the GNU General Public License is included with MoVE
# in the COPYING file, or see <https://www.gnu.org/licenses/>.
# --------------------------------------------------------------


from bokeh.layouts import column, widgetbox, layout
from bokeh.models import Button, Toggle, Label, HoverTool, Title
from bokeh.models import ColumnDataSource
from bokeh.models.widgets import Slider, Div
from bokeh.palettes import RdYlBu3
from bokeh.plotting import figure, curdoc
from bokeh.driving import count


from collections import deque
import numpy as np
import csv # DictWriter

import os # os.path.getsize() for logfile size
import sys
import socket
import msgpack
import threading
import queue
import time
from datetime import datetime
import pprint

# ------------------------------------------------------------------------------------------
# user-defined parameters
T_update = 50 # (ms) browser update rate specified in add_periodic_callback()
#T_update = 10 # (ms)
nMax=100 # initial number of points to display at a time; overridden by sliderNbuf.value when data arrives



# setup udp listener
#udp_ip = "127.0.0.1"
udp_ip = "0.0.0.0" # listen from anyone
udp_port = 8000
debug=1 # (0/1)
printState=1 # (0/1)
print('using udp_port={0}'.format(udp_port))


data = {'t': [],
        'Mx': [],
        'My': [],
        'Mz': [] }
ds = ColumnDataSource(data=data)


tNowFname=datetime.now()
myStr=tNowFname.strftime('%Y_%m_%d__%H_%M_%S') # strftime() = string format time: str='2018_07_24__18_18_40'
logFname='./{0}_data.csv'.format(myStr) # ./2021_07_20__19_06_43_data.csv
print('opening log file: {0}'.format(logFname))
fd=open(logFname,'a') # open vehicle model log file for subsequent appending


# ------------------------------------------------------------------------------
# the udp_receiver() thread enters a while loop to stay alive and block until it receives udp data
def udp_receiver(sock,e,eLog,qData,fd,debug):
    global data
    print('udp_receiver(): Starting udp_receiver thread')
    print('udp_receiver(): socket timeout is: {0}'.format( sock.gettimeout() ) )
    cntUdp=0
    tStart = time.time()
    
    writer = csv.DictWriter(fd, fieldnames=data.keys())
    writer.writeheader() # text descriptions of each csv field
    
    while e.is_set() is False:
        try:
            data, addr = sock.recvfrom(1024) # blocking udp receive; buffer size is 1024 bytes
            if len(data)>0:
                cntUdp += 1
                msg = msgpack.unpackb(data)
                if ((cntUdp%nMax==0) and (debug>0)):
                    print("udp_receiver(): received [{0}] bytes from [{1}], recv cntUdp={2}".format(len(data),addr,cntUdp) ,end='')
                    print(",   ".join(" {0}:{1:<0.3f}".format(k, v) for k, v in msg.items())) # print dict with formatted floats
                    
                qData.put( msg ) # put shared data on the que so main() or another thread can get it (make sure to remove this data or que will fill)
                
                if eLog.is_set():
                    #print('logging msg={0}'.format(msg))
                    writer.writerow(msg) # data keys must match msg keys
                    
        except socket.timeout:
            pass
        
        #print("{0}: len(qData)={1}".format(time.time()-tStart, qData.qsize()) )
    print('udp_receiver(): thread exiting')

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) # udp/ip
sock.settimeout(0.1) # responsive exit upon ctrl-c, but still a non-busy wait in the thread
sock.bind((udp_ip, udp_port)) # must bind() to listen
print("udp_receiver() thread listneing for udp on port [", udp_port, "]")

e        = threading.Event() # event for exiting all threads
eLog     = threading.Event() # event to toggle logging to local file
#eLogFile = threading.Event() # event to chose new file (True) or append to existing file (False)
qData = queue.Queue() # the queue for passing data from udp receiver thread to main or other threads

t1 = threading.Thread(name='udp_recv', target=udp_receiver, args=(sock,e,eLog,qData,fd,debug))
t1.start()





# ------------------------------------------------------------------------------------------
# brief documentation
description = Div(text="""<b><code>streamingSine_v3.py</code></b> - display M channels of live streaming data with N samples in the browser window.""") #, width=800, height=100)

# ------------------------------------------------------------------------------------------
# dynamic updating title bar to know if your web page is running or not
screen_width=900 # pixels
uptime_bar = Div(background='lightskyblue' , width=screen_width, text="", render_as_text=True)

# callback for uptime bar
t0=datetime.now()
def callback_uptime():
    # dynamically update label text to know if it's running
    tNow = datetime.now()
    uptime_str      = 'app uptime [{0}]'.format( str(tNow-t0).split('.')[0] ) # truncate microseconds on uptime
    tNow_str        = 'Currently: [{0}]'.format( tNow.strftime("%c") )
    try:
        logfileSize_str = 'log file size: {0} (bytes)'.format( os.path.getsize(logFname) )
    except FileNotFoundError as err:
        print('barf! - file no longer available: {0}'.format(err))
        logfileSize_str = '[ missing!]'
    uptime_bar.text = tNow_str + ", " + uptime_str + ", " + logfileSize_str

cb_uptime  = curdoc().add_periodic_callback(callback_uptime, 1000) # (ms) update freq for uptime bar
# ------------------------------------------------------------------------------------------






# ------------------------------------------------------------------------------------------
# BEGIN: update_window()
# update_window(y,u,N) creates a FIFO window of the most recent data, moving in time as updates occur
#
#     this function accepts a deque list, y, and additional scalar data point u,
#     and appends u to deque list y until length reaches N.
#     When y is length N, drop the oldest in y and add u.
#
#     uses native python double ended queue's (deque's) to avoid creation of new
#     numpy arrays or shifting the entire python list each iteration
#
def update_window(y,u,N):
    
    if len(y)<N:
        y.append(u) # append to the right side of the deque list (this is the newest, or entering data)
    else:           
        y.append(u) # append 1 number to the right side (this is the newest, or entering data)
        y.popleft() # remove 1 number from the left side (this is the oldest, or exiting data)
    
    return y

# END: update_window()
# ------------------------------------------------------------------------------------------


# create a plot and style its properties
plot = figure( # x_range=(-10, 10), y_range=(-10, 10),
           tools="pan,xpan,ypan,hover,crosshair,box_zoom,reset,save",
           x_axis_label='time axis (s)', y_axis_label='Y-axis',
           height=500, width=900) # x_axis_type='datetime', 

plot.border_fill_color = 'white'
plot.background_fill_color = 'white'
plot.outline_line_color = 'LightBlue'
plot.grid.grid_line_color = 'LightBlue'

# add a text renderer to our plot
# http://bokeh.pydata.org/en/latest/docs/reference/models/annotations.html
label = Label(x=50, y=50, text=" cnt={0:d}, t={1:0.2f}(s)".format(0,0),
              text_font_size='11pt', border_line_color='black', background_fill_color='white',
              x_units='screen', y_units='screen',#level='glyph', render_mode='css',
              background_fill_alpha=0.8, text_color='blue') # text_color='#eeeeee'
plot.add_layout(label)


# add a circle renderer with a size, color, and alpha: http://bokeh.pydata.org/en/latest/docs/reference/plotting.html
Mx = plot.circle(x='t', y='Mx', size=4, color='red'  , fill_alpha=0.2, source=ds)
My = plot.circle(x='t', y='My', size=4, color='blue' , fill_alpha=0.2, source=ds)
Mz = plot.circle(x='t', y='Mz', size=4, color='green', fill_alpha=0.2, source=ds)


# make some sliders to vary sine wave magnitude, frequency, and vertical offset
sliderNbuf   = Slider(start=10, end=1000, value=5, step=2, title="Display N Namples")



# ------------------------------------------------------------------------------------------
# callback for da Start/Stop button, add_periodic_callback() is what makes this loop
cbId=[]
def callback_button_startstop():
    global t0, cbId
    if button_startstop.label == 'Press to: > Play':
        button_startstop.label = 'Press to: = Pause'
        cbId = curdoc().add_periodic_callback(my_periodic_callback, T_update) # <-- this controls update frequency
        #t0=time.time() # (s) reset base time, t0, each time button_startstop is pressed
    else:
        button_startstop.label = 'Press to: > Play'
        try:
            curdoc().remove_periodic_callback(cbId)
        except ValueError as err:
            print('oops - button_startstop barf: {0}'.format(err))
# ------------------------------------------------------------------------------------------

# callback for logging button
def callback_button_log():
    if button_log.label == ' Press to START logging udp data ':
        button_log.label = '[Press to STOP  logging udp data]'
        eLog.set() # make eLog.isset() == True
        button_log.button_type='danger' # make it red and scary to remind you're writing udp to a file
    else:
        button_log.label = ' Press to START logging udp data '
        eLog.clear() # make eLog.isset() == False
        button_log.button_type='success' # make it green to indicate safety; no longer writing udp log file

# callback for logfile decision: same file or new?
#def callback_button_logfile():
#    if button_logfile.label == 'create a new file':
#        button_logfile.label = 'append to same file'
#        eLogFile.clear() # make eLog.isset() == False
#        button_logfile.button_type='danger' # make it red and scary to remind that logging will append to the same file
#    else:
#        button_logfile.label = 'create a new file'
#        eLogFile.set() # make eLog.isset() == True
#        button_logfile.button_type='success' # make it green to indicate safety; log to a new file


# ------------------------------------------------------------------------------------------
# initialize an empty double-ended queue to grow x and y arrays as new data comes in
new_data_t  = deque([])
new_data_Mx = deque([])
new_data_My = deque([])
new_data_Mz = deque([])

cnt = 0
tDataNow=0
tDataLast=0

@count()
# create a callback that will add a number from a networked data source (this example creates y=A*sin(omega*t) )
def my_periodic_callback(t):
    global cnt,new_data_t,new_data_Mx,new_data_My,new_data_Mz,tNow
    global tDataNow,tDataLast
            
    cnt     = cnt + 1
    #tNow  = time.time()-t0 # (s) system time, real wall-clock time in seconds since the epoch, minus t0 is elapsed time in seconds
    
    nMax = sliderNbuf.value
    
    newData=False
    tDataLast = tDataNow
    skipCnt=0
    while qData.empty()==False:
        data      = qData.get() # non-blocking
        tDataNow  = data['t']  # eliminates backlog; keeps the last point
        Mx        = data['Mx']
        My        = data['My']
        Mz        = data['Mz']
        newData   = True
        skipCnt  += 1         # keep track of how many points were in the udp queue
    
    if newData==True:
        
        # create a new dictionary each loop iteration
        new_data = dict()
        
        #y = update_window(y,u,N) # grow a deque to size N then update as a FIFO buffer for a sliding window with newest data
        new_data_t  = update_window(new_data_t ,tDataNow,nMax) # append the 't' to new_data_x deque
        new_data_Mx = update_window(new_data_Mx,Mx      ,nMax) # append this loop's y value to new_data_y deque
        new_data_My = update_window(new_data_My,My      ,nMax) # append this loop's y value to new_data_y deque
        new_data_Mz = update_window(new_data_Mz,Mz      ,nMax) # append this loop's y value to new_data_y deque
        
        while len(new_data_t) >nMax: new_data_t.popleft()
        while len(new_data_Mx)>nMax: new_data_Mx.popleft()
        while len(new_data_My)>nMax: new_data_My.popleft()
        while len(new_data_Mz)>nMax: new_data_Mz.popleft()
        
        new_data['t'] = list(new_data_t) # convert double-ended queue into a list, stored as a new dict() entry
        new_data['Mx']    = list(new_data_Mx)
        new_data['My']    = list(new_data_My)
        new_data['Mz']    = list(new_data_Mz)
        
        ds.data = new_data # BEST PRACTICE --- update .data in one step with a new dict
        
        # dynamically update label text
        dt = tDataNow-tDataLast # dt for data
        label.text = " cnt={0:10d}, t_elap={1:10.3f}(s), dt_display={2:10.3f}(s), N_display={3}, t_window={4:6.2f}(s) , skipCnt={5} , dt_data={6:6.2E}" \
                    .format(cnt, time.time()-t0.timestamp() ,dt,nMax,nMax*dt,skipCnt,dt/skipCnt)
                    #        0    1   2   3     4       5       6
# ------------------------------------------------------------------------------------------


# add a button widget and configure with the call back
button_startstop = Button(label='Press to: > Play', width=100)
button_startstop.on_click(callback_button_startstop)

# add a button for logging
button_log = Button(label=' Press to START logging udp data ', width=100)
button_log.on_click(callback_button_log)

# add a button for logfile decision: same or new?
#button_logfile = Button(label='log to same file', width=100)
#button_logfile.on_click(callback_button_logfile)


## put the button and plot in a layout and add to the document
layout = layout([
    [description],
    [uptime_bar],
    [column(button_startstop)],
    [column(button_log)],#[column(button_file)],
    [column(sliderNbuf)],
    [plot]
    ], sizing_mode='stretch_width') # "fixed", "stretch_both", "scale_width", "scale_height", "scale_both"
curdoc().add_root(layout)
curdoc().title = "streamingSine example"
